#include <initializer_list>
#include <type_traits>
#include <tr2/type_traits>
#include <tuple>
#include <iostream>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/base_object.hpp>

template <typename Archive, typename T> void __serialize(Archive &ar, T &v) {
  ar &v;
}

template <typename Archive, typename T, typename... Args>
void __serialize(Archive &ar, T &v, Args &... args) {
  ar &v;
  __serialize(ar, args...);
}

template <typename Archive, typename CurrentClass, typename typelist>
struct __SERIALIZEBASE {
  typedef typename typelist::first::type first;
  typedef typename typelist::rest::type rest;
  inline static void exec(Archive &ar, CurrentClass &cc) {
    ar &boost::serialization::base_object<first>(cc);
    __SERIALIZEBASE<Archive, CurrentClass, rest>::exec(ar, cc);
  };
};

template <typename Archive, typename CurrentClass>
struct __SERIALIZEBASE<Archive, CurrentClass,
                       std::tr2::__reflection_typelist<> > {
  inline static void exec(Archive &ar, CurrentClass &cc) {
    // do nothing
  };
};

#define SERIALIZE(...)                                                         \
  friend class boost::serialization::access;                                   \
  template <class Archive>                                                     \
  void serialize(Archive &ar, const unsigned int version) {                    \
    using currentClass = std::remove_reference<decltype(*this)>::type;         \
    using BaseTypeList = std::tr2::direct_bases<currentClass>::type;           \
    __SERIALIZEBASE<Archive, currentClass, BaseTypeList>::exec(ar, *this);     \
    __serialize(ar, __VA_ARGS__);                                              \
  }
